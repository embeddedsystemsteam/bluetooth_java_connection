This is the README for the code that connects bluetooth to a java program.

Instructions to set up the libraries along with the main loop:
http://playground.arduino.cc/Interfacing/Java

Libraries:
http://rxtx.qbang.org/wiki/index.php/Download

I haven’t gotten the libraries to link properly. The code is currently executing with code 1, but Tanner Juby got it to work so I’m going to keep at it and potentially reach out to him if I can’t get it working soon. If anyone has any insight into linking the libraries please let me know.